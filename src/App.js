import "./App.css";
import HomePage from "./containers/homePage/index";
function App() {
  return (
    <div className="App">
      <HomePage />
    </div>
  );
}

export default App;

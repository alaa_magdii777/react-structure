import React from "react";
import styled from "styled-components";
import OurServices from "../components/ourServices";
import rocket_launch from "../assets/pictures/rocket_launch.png";
import { Marginer } from "../components/marginer";
import SectionTitle from "../components/sectionTitle";
import { Element } from "react-scroll";

const AboutUsContainer = styled(Element)`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding-bottom: 7em;
`;

export default function AboutUsSection(props) {
  return (
    <AboutUsContainer name="AboutUsSection">
      <Marginer direction="vertical" margin="50px" />
      <SectionTitle>About us</SectionTitle>
      <OurServices
        title={"Quality is our priority"}
        imgUrl={rocket_launch}
        details={
          "Beema is about designing, building and deliverying best quality software for your company. We make sure that you get the best software inferstracture and set of applications to ensure the best experience of your clients. So wether you are handling thousands of payment transactions or you’re just starting out, you are in the right place. "
        }
      />
    </AboutUsContainer>
  );
}

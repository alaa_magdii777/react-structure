import styled from "styled-components";
import React from "react";
import { Marginer } from "../components/marginer";
import SectionTitle from "../components/sectionTitle";
import { Element } from "react-scroll";
import { CarouselProvider, Slider, Slide, DotGroup } from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import ReviewCard from "../components/reviewCard";
import Themes from "../themes";
import { useMediaQuery } from "react-responsive";
import vector1 from "../assets/pictures/vector-1.png";
import vector2 from "../assets/pictures/vector-2.png";

const ReviewsContainer = styled(Element)`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-bottom: 8em;
  /* height: 00px; */
`;

const StyledCarouselProvider = styled(CarouselProvider)`
  width: 80%;
  @media screen and (max-width: 480px) {
    width: 100%;
  }
`;

const StyledSlide = styled(Slide)`
  .carousel__inner-slide {
    display: flex;
    justify-content: center;
  }
`;

const StyledDotGroup = styled(DotGroup)`
  margin: -180px 0px 0px 0px;
  /* padding: -180px 0px 0px 0px; */
  display: flex;
  justify-content: center;
  button {
    width: 11px;
    height: 11px;
    border-radius: 50%;
    background-color: ${Themes.lightGray};
    border: none;
    outline: none;
    &:not(:last-of-type) {
      margin-right: 3px;
    }
  }
  .carousel__dot--selected {
    background-color: ${Themes.darkGray};
  }
`;

export default function ReviewSection(props) {
  const isMobile = useMediaQuery({ query: "(max-width: 480px)" });
  return (
    <ReviewsContainer name="ReviewsSection">
      <SectionTitle>Reviews</SectionTitle>
      <Marginer direction="vertical" margin="4em" />
      <StyledCarouselProvider
        // naturalSlideHeight={300}
        naturalSlideWidth={200}
        totalSlides={4}
        visibleSlides={isMobile ? 1 : 2}
      >
        <Slider>
          <StyledSlide index={0}>
            <ReviewCard
              userName={"Alaa Magdy"}
              vectorUrl={vector1}
            ></ReviewCard>
          </StyledSlide>
          <StyledSlide index={1}>
            <ReviewCard
              userName={"Abdelrahman Magdy"}
              vectorUrl={vector2}
            ></ReviewCard>
          </StyledSlide>
          <StyledSlide index={2}>
            <ReviewCard
              userName={"Abdelrahman Magdy"}
              vectorUrl={vector2}
            ></ReviewCard>
          </StyledSlide>
          <StyledSlide index={3}>
            <ReviewCard
              userName={"Alaa Magdy"}
              vectorUrl={vector1}
            ></ReviewCard>
          </StyledSlide>
        </Slider>
        <StyledDotGroup />
      </StyledCarouselProvider>
      {/* <Marginer direction="vertical" margin="4em" /> */}
    </ReviewsContainer>
  );
}

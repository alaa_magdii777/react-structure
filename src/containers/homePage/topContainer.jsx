import React from "react";
import styled from "styled-components";
import BackGroundImg from "../../assets/pictures/company.png";
import Button from "../../components/button";
import Logo from "../../components/logo";
import { Marginer } from "../../components/marginer";
import Themes from "../../themes";
import FontIcon from "../../components/fontIcon";
// import NavBar from "../../components/navBar";
import { Element, scroller } from "react-scroll";

const TopSection = styled.div`
  width: 100%;
  height: 100vh;
  padding: 0;
  background-image: url(${BackGroundImg});
  position: relative;
`;

const BackgroundFilter = styled.div`
  width: 100%;
  height: 100%;
  background-color: rgba(60, 60, 60, 0.89);
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Content = styled.h1`
  font-size: 34px;
  color: ${Themes.white};
  margin: 0;
  line-height: 1.4;
  font-weight: 500;
`;
const ArrowContainer = styled.div`
  position: absolute;
  bottom: 20px;
  left: 50%;
  transform: translateX(-50%);
`;

const ScrollToNextSection = () => {
  scroller.scrollTo("ServiceSection", { smooth: true, duration: 1500 });
};

export default function TopContainer(props) {
  return (
    <Element>
      <TopSection>
        <BackgroundFilter>
          {/* <NavBar /> */}
          <Marginer direction="vertical" margin="4em" />
          <Logo />
          <Marginer direction="vertical" margin="4em" />
          <Content>Software Development</Content>
          <Content>From the best in the industry</Content>
          <Marginer direction="vertical" margin="2em" />
          <Button bgColor={`${Themes.primary}`}>Start your Project</Button>
          <Marginer direction="vertical" margin="2em" />
          <ArrowContainer>
            <FontIcon onClick={ScrollToNextSection} />
          </ArrowContainer>
          <Marginer direction="vertical" margin="4em" />
        </BackgroundFilter>
      </TopSection>
    </Element>
  );
}

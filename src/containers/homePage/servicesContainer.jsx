import React from "react";
import styled from "styled-components";
import OurServices from "../../components/ourServices";
import ImgService from "../../assets/pictures/serviceImg.png";
import ContactUs from "../../assets/pictures/mobile_phone.png";
import web_development from "../../assets/pictures/web_development.png";
import { Marginer } from "../../components/marginer";
import SectionTitle from "../../components/sectionTitle";
import { Element } from "react-scroll";

const ServiceContainer = styled(Element)`
  width: 100%;
  /* min-height: 1400px; */
  display: flex;
  flex-direction: column;
  padding-bottom: 7em;
`;

export default function ServicesContainer(props) {
  return (
    <ServiceContainer name="ServiceSection">
      <Marginer direction="vertical" margin="50px" />
      <SectionTitle>Best Quality Software</SectionTitle>
      <OurServices
        title={"Fully integrated services"}
        details={
          "We build and deliver fully integrated webapps with customized control panels that fit your compnay needs"
        }
        imgUrl={ImgService}
      />
      <OurServices
        isReverse
        title={"mobile_phone"}
        imgUrl={ContactUs}
        details={
          "We build and deliver fully integrated webapps with customized control panels that fit your compnay needs"
        }
      />
      <OurServices
        title={"Quality is our priority"}
        imgUrl={web_development}
        details={
          "We have teams of professional developers, designers and managers that ensures delivering the best software quality for your company"
        }
      />
    </ServiceContainer>
  );
}

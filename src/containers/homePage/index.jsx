import React from "react";
import TopContainer from "./topContainer";
import FooterContainer from "./footerContainer";
import ServicesContainer from "./servicesContainer";
import styled from "styled-components";
import ReviewSection from "../reviewSection";
import AboutUsSection from "../aboutUsSection";

const HomePageContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
`;

export default function HomePage(props) {
  return (
    <HomePageContainer>
      <TopContainer />
      <ServicesContainer />
      <ReviewSection />
      <AboutUsSection />
      <FooterContainer />
    </HomePageContainer>
  );
}

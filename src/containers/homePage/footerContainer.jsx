import styled from "styled-components";
import React from "react";
import Themes from "../../themes";
import { Element } from "react-scroll";
// import Button from "../../components/button";
import Logo from "../../components/logo";
import { Marginer } from "../../components/marginer";

const StyledFooter = styled(Element)`
  width: 100%;
  height: 482px;
  background-color: ${Themes.anotherDark};
  align-items: center;
  display: flex;
  flex-direction: column;
`;
const Content = styled.h1`
  font-size: 30px;
  color: ${Themes.white};
  margin: 0;
  line-height: 1.4;
  font-weight: 500;
`;

export default function FooterContainer(props) {
  return (
    <StyledFooter name="FooterContainer">
      <Marginer direction="vertical" margin="4em" />
      <Logo />
      <Marginer direction="vertical" margin="4em" />
      <Content>Software Development</Content>
      <Content>From the best in the industry</Content>
      <Marginer direction="vertical" margin="2em" />
      {/* <Button bgColor={`${Themes.primary}`}>Start your Project</Button> */}
    </StyledFooter>
  );
}

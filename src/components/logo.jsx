import React from "react";
import styled from "styled-components";
import Themes from "../themes";
import { FaReact } from "react-icons/fa";

// const LogoImg = styled.img`
//   width: ${({ inline }) => (inline ? "40px" : "131px")};
//   height: ${({ inline }) => (inline ? "40px" : "131px")};
// `;

const LogoText = styled.div`
  font-size: ${({ inline }) => (inline ? "20px" : "52px")};
  color: ${({ inline }) => (inline ? `${Themes.white}` : `${Themes.primary}`)};
  font-weight: 900;
  margin: ${({ inline }) => (inline ? "0 10px" : "0px")};
`;

const LogoContainer = styled.div`
  display: flex;
  flex-direction: ${({ inline }) => (inline ? "row" : "column")};
  align-items: center;
`;
const LogoIcon = styled.div`
  font-size: 120px;
  color: ${Themes.primary};
`;
export default function Logo(props) {
  const { inline } = props;
  return (
    <LogoContainer inline={inline}>
      {/* <LogoImg src={AnotherLogo} inline={inline} /> */}
      <LogoIcon>
        <FaReact />
      </LogoIcon>
      <LogoText inline={inline}>React</LogoText>
    </LogoContainer>
  );
}

import React from "react";
import styled from "styled-components";
import Themes from "../../themes";
import Button from "../button";
import Logo from "../logo";
import { Marginer } from "../marginer";

const NavBarContainer = styled.div`
  display: flex;
  height: 65px;
  width: 100%;
  padding: 0 1em;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;

const BrandContainer = styled.div`
  flex-direction: row;
  /* transform: translateX(-35em); */
  /* //display: flex; */
  /* align-items: flex-end;
  align-self: flex-end;
  align-content: flex-end; */
`;

export default function NavBar(props) {
  return (
    <NavBarContainer>
      <Marginer direction="vertical" margin="6px" />
      {/* <BrandContainer> */}
      <Logo inline />
      {/* </BrandContainer> */}
      <BrandContainer>
        <Button small bgColor={`${Themes.primary}`}>
          Get Started
        </Button>
        <Marginer direction="horizontal" margin="6px" />
        <Button small bgColor={`${Themes.transparent}`}>
          Get Started
        </Button>
      </BrandContainer>
    </NavBarContainer>
  );
}

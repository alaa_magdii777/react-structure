import styled from "styled-components";
import Themes from "../../themes";

const Title = styled.h2`
  font-size: 28px;
  color: ${Themes.black};
  font-weight: 700;
  margin: 0px 5px;
`;
const Details = styled.p`
  font-size: 19px;
  color: ${Themes.gray};
  text-align: center;
  max-width: 70%;
`;
const DescriptionContainer = styled.div`
  flex-direction: column;
  display: flex;
  max-width: 60%;
  align-items: center;
`;
const Img = styled.img`
  width: 338px;
  height: 201px;
`;
const OurServicesContainer = styled.div`
  display: flex;
  flex-direction: ${({ isReverse }) => isReverse && "row-reverse"};
  padding: 100px 0px 0px 0px;
  /* padding: ${({ isReverse }) => isReverse && "100px 0px 0px 0px;"}; */
`;

export default function OurServices(props) {
  const { title, details, imgUrl, isReverse } = props;
  return (
    <OurServicesContainer isReverse={isReverse}>
      <DescriptionContainer>
        <Title>{title}</Title>
        <Details>{details}</Details>
      </DescriptionContainer>
      <Img src={imgUrl} />
    </OurServicesContainer>
  );
}

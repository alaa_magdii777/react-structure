import styled from "styled-components";
import Themes from "../../themes";

const SectionTitle = styled.h1`
  font-size: 34px;
  color: ${Themes.black};
  font-weight: bold;
  padding: 0px 0px 50px 0px;
`;

export default SectionTitle;

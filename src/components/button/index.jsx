import React from "react";
import Themes from "../../themes";
import styled from "styled-components";

const ButtonContainer = styled.button`
  display: flex;
  padding: 7px 15px;
  border-radius: 5px;
  color: ${Themes.white};
  font-weight: bold;
  font-size: ${({ small }) => (small ? "10px" : "16px")};
  border: 2px solid transparent;
  background-color: ${({ bgColor }) => bgColor};
  &:hover {
    transition: all 220ms ease-in-out;
    background-color: transparent;
    border: 2px solid ${Themes.primary};
  }
`;
export default function Button(props) {
  return <ButtonContainer {...props}>{props.children}</ButtonContainer>;
}

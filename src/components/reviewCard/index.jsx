import styled from "styled-components";
import React from "react";
import Themes from "../../themes";
import { faQuoteLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const CardContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 522px;
  background-color: ${Themes.white};
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.2);
  border-radius: 3px;
  margin: 5px 2em;
  border: 0.5px;
  justify-content: center;
`;
const ReviewDetails = styled.p`
  font-size: 17px;
  color: ${Themes.gray};
  text-align: center;
  padding: 11px 11px 11px 11px;
  align-items: center;
  height: 55%;
`;

const ContainerBottom = styled.div`
  height: 30%;
`;

const QuoteContainer = styled.div`
  display: flex;
  color: #d1d1d1;
  font-size: 35px;
  padding: 20px;
  align-items: flex-start;
  height: 15%;
`;

const Vector = styled.img`
  width: 70px;
  height: 70px;
  border-radius: 50%;
  margin-bottom: 60px;
  margin-right: 20px;
  margin-left: 20px;
`;
const Line = styled.span`
  height: 0px;
  width: 100%;
  border: 1px solid#CDCDCD;
  display: flex;
  margin: 20px 0px 20px 0px;
`;

const UserNameContainer = styled.p`
  font-size: 17px;
  color: ${Themes.black};
  font-weight: 900px;
`;
const JobTitleContainer = styled.p`
  font-size: 17px;
  color: ${Themes.primary};
  font-weight: 900px;
  margin: 0px;
  padding: 0px;
`;
const PersonalContainer = styled.div`
  display: flex;
`;
const PersonContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

export default function ReviewCard(props) {
  const { vectorUrl, userName } = props;
  return (
    <CardContainer>
      <QuoteContainer>
        <FontAwesomeIcon icon={faQuoteLeft} />
      </QuoteContainer>
      <ReviewDetails>
        I very much enjoyed working with Beema and the team - they have an
        excellent grasp of their subject, and have created something great for
        us.
      </ReviewDetails>
      <ContainerBottom>
        <Line />
        <PersonalContainer>
          <Vector src={vectorUrl} />
          <PersonContent>
            <UserNameContainer>{userName}</UserNameContainer>
            <JobTitleContainer>Developer</JobTitleContainer>
          </PersonContent>
        </PersonalContainer>
      </ContainerBottom>
    </CardContainer>
  );
}
